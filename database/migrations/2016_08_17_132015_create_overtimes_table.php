<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvertimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overtimes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('overtime_type_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->date('date');
            $table->time('clock_in');
            $table->time('clock_out');
            $table->boolean('approved')->nullable();
            $table->integer('created_by')->unsigned();
            $table->foreign('overtime_type_id')->references('id')->on('overtime_types');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('created_by')->references('id')->on('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('overtimes');
    }
}
