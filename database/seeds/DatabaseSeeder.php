<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Employee;
use App\Company;
use App\Client;
use App\Project;
use App\overtime_type;
use App\Overtime;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        DB::table('employees')->delete();
        DB::table('departments')->delete();
        DB::table('companies')->delete();
        DB::table('clients')->delete();
        DB::table('projects')->delete();
        
        $departments = array(
            ['id' => 3,'department_name' => 'Sistem Integrator','prefix' => 'SID'],
            ['id' => 4,'department_name' => 'Admin','prefix' => 'ADM'],
            ['id' => 5,'department_name' => 'Accounting','prefix' => 'ACC'],
            ['id' => 7,'department_name' => 'PVN','prefix' => 'PVN'],
            ['id' => 8,'department_name' => 'Solution','prefix' => 'SOL'],
            ['id' => 9,'department_name' => 'HRD','prefix' => 'HRD'],
            ['id' => 10,'department_name' => 'Creative','prefix' => 'CRD'],
            ['id' => 13,'department_name' => 'System Administrator','prefix' => 'SYS'],
            ['id' => 14,'department_name' => 'General Affair','prefix' => 'GAD'],
            ['id' => 16,'department_name' => 'Docoblast','prefix' => 'DOCOBLAST'],
            ['id' => 17,'department_name' => 'Produk','prefix' => 'PRD'],
        );
        
        foreach ($departments as $department)
        {
            Department::create($department);
        }
        
        $employees = array(
            [
                'id' => 6,
                'name' => 'Feri Harsanto',
                'birth_place' => 'Solo',
                'birth_date' => '1984-02-10',
                'join_date' => '2012-09-05',
                'position' => 'CTO',
                'department_id' => 8,
                'employee_id' => NULL,
                'email' => 'feri.harsanto@docotel.co.id',
                'username' => 'feri.harsanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 11,
                'name' => 'Yuno Lastyanto',
                'birth_place' => 'Cilacap',
                'birth_date' => '1981-01-03',
                'join_date' => '2012-05-09',
                'position' => 'Art Director',
                'department_id' => 10,
                'employee_id' => NULL,
                'email' => 'yuno.lastyanto@docotel.co.id',
                'username' => 'yuno.lastyanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 16,
                'name' => 'Candra',
                'birth_place' => 'Bagan Siapi Api',
                'birth_date' => '1989-10-17',
                'join_date' => '2014-04-01',
                'position' => 'Tukang bersih-bersih',
                'department_id' => 17,
                'employee_id' => NULL,
                'email' => 'candra@docotel.co.id',
                'username' => 'candra',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 34,
                'name' => 'Meylando LA Ritonga',
                'birth_place' => 'Jakarta',
                'birth_date' => '1981-04-03',
                'join_date' => '2011-10-10',
                'position' => 'Leader SI',
                'department_id' => 3,
                'employee_id' => NULL,
                'email' => 'lando@docotel.com',
                'username' => 'meylando.la',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 36,
                'name' => 'Debora Siregar',
                'birth_place' => 'Medan',
                'birth_date' => '1979-01-25',
                'join_date' => '2014-11-03',
                'position' => 'Head of GA',
                'department_id' => 14,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'debora.siregar',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 44,
                'name' => 'Mustika Rengganingrum',
                'birth_place' => 'Bandung',
                'birth_date' => '1992-06-10',
                'join_date' => '2015-06-01',
                'position' => 'HR Generalist',
                'department_id' => 9,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'mustika.rengganingrum',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 45,
                'name' => 'Wahyu Tri Putra',
                'birth_place' => 'Jakarta',
                'birth_date' => '1992-08-03',
                'join_date' => '2015-07-27',
                'position' => 'SQL',
                'department_id' => 8,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'wahyu.tri',
                'password' => Hash::make('rahasia')
            ] ,            
            [
                'id' => 49,
                'name' => 'Vian Hidayat',
                'birth_place' => 'Jakarta',
                'birth_date' => '1981-01-23',
                'join_date' => '2015-09-01',
                'position' => 'SQL',
                'department_id' => 8,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'vian.hidayat',
                'password' => Hash::make('rahasia')
            ] ,            
            [
                'id' => 52,
                'name' => 'Muammar',
                'birth_place' => 'Bima',
                'birth_date' => '1982-09-23',
                'join_date' => '2015-11-02',
                'position' => 'Sys Admin Leader',
                'department_id' => 13,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'muammar',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 74,
                'name' => 'Aryo Darmo Kusuma',
                'birth_place' => 'Sumenep',
                'birth_date' => '1980-10-16',
                'join_date' => '2011-12-05',
                'position' => 'Direktur Solution',
                'department_id' => 8,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'aryo.darmo',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 87,
                'name' => 'Steffi Novratilova',
                'birth_place' => 'Koto Tuo',
                'birth_date' => '1992-11-26',
                'join_date' => '2016-03-01',
                'position' => 'Project Admin',
                'department_id' => 4,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'steffi.novratilova',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 88,
                'name' => 'Clau Diana',
                'birth_place' => 'Jakarta',
                'birth_date' => '1995-02-13',
                'join_date' => '2016-03-07',
                'position' => 'Project Admin',
                'department_id' => 4,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'clau.diana',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 90,
                'name' => 'Wakhuri',
                'birth_place' => 'Brebes',
                'birth_date' => '1979-10-26',
                'join_date' => '2016-03-21',
                'position' => 'driver',
                'department_id' => 14,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'wakhuri',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 103,
                'name' => 'C Adityo Prastowo',
                'birth_place' => 'Jakarta',
                'birth_date' => '1980-03-24',
                'join_date' => '2016-04-11',
                'position' => 'Driver',
                'department_id' => 14,
                'employee_id' => NULL,
                'email' => NULL,
                'username' => 'c.adityo',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 8,
                'name' => 'Alit Irianto',
                'birth_place' => 'Jakarta',
                'birth_date' => '1982-03-12',
                'join_date' => '2012-03-01',
                'position' => 'System Integrator',
                'department_id' => 13,
                'employee_id' => 52,
                'email' => 'alit.irianto@docotel.co.id',
                'username' => 'alit.irianto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 9,
                'name' => 'Nura Aditya Permana',
                'birth_place' => 'Bandung',
                'birth_date' => '1986-10-01',
                'join_date' => '2012-03-28',
                'position' => 'Team Leader Solution',
                'department_id' => 8,
                'employee_id' => 6,
                'email' => 'nura.aditya@docotel.co.id',
                'username' => 'nura.aditya',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 10,
                'name' => 'Dedy Surya Nugroho',
                'birth_place' => 'Jakarta',
                'birth_date' => '1984-12-12',
                'join_date' => '2012-04-02',
                'position' => 'Graphic Designer',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => 'dedy.surya@docotel.co.id',
                'username' => 'dedy.surya',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 13,
                'name' => 'Nur Alam',
                'birth_place' => 'Bone',
                'birth_date' => '1990-01-07',
                'join_date' => '2014-02-13',
                'position' => 'Lead Programmer',
                'department_id' => 8,
                'employee_id' => 9,
                'email' => 'nur.alam@docotel.co.id',
                'username' => 'nur.alam',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 14,
                'name' => 'Edward Yanuarius',
                'birth_place' => 'Jakarta',
                'birth_date' => '1984-01-23',
                'join_date' => '2014-03-03',
                'position' => 'Lead Programmer',
                'department_id' => 8,
                'employee_id' => 9,
                'email' => 'edward.yanuarius@docotel.co.id',
                'username' => 'edward.yanuarius',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 23,
                'name' => 'Rusman Jaya',
                'birth_place' => 'Cirebon',
                'birth_date' => '1986-04-23',
                'join_date' => '2014-11-03',
                'position' => 'Lead Programmer',
                'department_id' => 8,
                'employee_id' => 9,
                'email' => 'rusman.jaya@docotel.co.id',
                'username' => 'rusman.jaya',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 17,
                'name' => 'M. Edi Sujarwo',
                'birth_place' => 'Bogor',
                'birth_date' => '1994-06-04',
                'join_date' => '2014-06-02',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 23,
                'email' => 'edi.sujarwo@docotel.co.id',
                'username' => 'edi.sujarwo',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 22,
                'name' => 'Ardyanto',
                'birth_place' => 'Jakarta',
                'birth_date' => '1988-06-07',
                'join_date' => '2014-11-03',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 14,
                'email' => 'ardyanto@docotel.co.id',
                'username' => 'ardyanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 24,
                'name' => 'Claudia Clarentia Ciptohartono',
                'birth_place' => 'Semarang',
                'birth_date' => '1992-02-24',
                'join_date' => '2015-02-02',
                'position' => 'Art Creative',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => 'claudia.clarentia@docotel.co.id',
                'username' => 'claudia.clarentia',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 25,
                'name' => 'Danang Hadi Prayogo',
                'birth_place' => 'Ngawi',
                'birth_date' => '1993-05-25',
                'join_date' => '2015-02-09',
                'position' => 'IT Support',
                'department_id' => 13,
                'employee_id' => 52,
                'email' => 'danang.hadi@docotel.co.id',
                'username' => 'danang.hadi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 26,
                'name' => 'Meyta Zenis Taliti',
                'birth_place' => 'Bogor',
                'birth_date' => '1995-05-14',
                'join_date' => '2015-02-26',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 6,
                'email' => 'meyta.zenis@docotel.co.id',
                'username' => 'meyta.zenis',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 28,
                'name' => 'Linggar Budi Prasetyo',
                'birth_place' => 'Tangerang',
                'birth_date' => '1991-06-14',
                'join_date' => '2015-08-14',
                'position' => 'Graphic Desiner',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => 'linggar.budi@docotel.co.id',
                'username' => 'linggar.budi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 31,
                'name' => 'Nurfirliana Muzanella',
                'birth_place' => 'Tegal',
                'birth_date' => '1996-04-10',
                'join_date' => '0000-00-00',
                'position' => 'Front-end Developer',
                'department_id' => 8,
                'employee_id' => 13,
                'email' => NULL,
                'username' => 'nurfirliana.muzanella',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 32,
                'name' => 'Yhose Christ Widtantio',
                'birth_place' => 'Brebes',
                'birth_date' => '1992-09-23',
                'join_date' => '2015-03-01',
                'position' => 'Programer',
                'department_id' => 8,
                'employee_id' => 13,
                'email' => NULL,
                'username' => 'yhose.christ',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 33,
                'name' => 'Erick Rizal',
                'birth_place' => 'Muara Badak',
                'birth_date' => '1991-10-03',
                'join_date' => '2014-04-01',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 23,
                'email' => NULL,
                'username' => 'erick.rizal',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 35,
                'name' => 'Risma Fitriani',
                'birth_place' => 'Pekalongan',
                'birth_date' => '1993-03-15',
                'join_date' => '2013-11-20',
                'position' => 'Project Admin',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'risma.fitriani',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 37,
                'name' => 'Elyyus Sunarsa',
                'birth_place' => 'Tangerang',
                'birth_date' => '1993-07-30',
                'join_date' => '2014-11-14',
                'position' => 'Pre- Sales',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'elyyus.sunarsa',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 38,
                'name' => 'Herwan Setiaji',
                'birth_place' => 'Jakarta',
                'birth_date' => '1981-04-18',
                'join_date' => '2012-02-20',
                'position' => 'Pre- Sales',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'herwan.setiaji',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 40,
                'name' => 'Lanny Sutanto',
                'birth_place' => 'Surabaya',
                'birth_date' => '1991-02-04',
                'join_date' => '2015-04-01',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 23,
                'email' => NULL,
                'username' => 'lanny.sutanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 41,
                'name' => 'Sheani Octavia',
                'birth_place' => 'Ciamis',
                'birth_date' => '1990-10-28',
                'join_date' => '2015-03-02',
                'position' => 'Creative Content',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => NULL,
                'username' => 'sheani.octavia',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 46,
                'name' => 'Abdullah Kariim',
                'birth_place' => 'Bogor',
                'birth_date' => '1996-02-15',
                'join_date' => '2015-08-10',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 14,
                'email' => NULL,
                'username' => 'abdullah.kariim',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 48,
                'name' => 'Ali Ikhwan',
                'birth_place' => 'Pati',
                'birth_date' => '1989-01-01',
                'join_date' => '2015-09-01',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 13,
                'email' => NULL,
                'username' => 'ali.ikhwan',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 50,
                'name' => 'Nuryadi Santoso',
                'birth_place' => 'Tangerang',
                'birth_date' => '1992-02-27',
                'join_date' => '2015-09-07',
                'position' => 'System Administrator',
                'department_id' => 13,
                'employee_id' => 52,
                'email' => NULL,
                'username' => 'nuryadi.santoso',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 54,
                'name' => 'Fetriant Arief Muhammad',
                'birth_place' => 'Jakarta',
                'birth_date' => '1994-04-14',
                'join_date' => '2015-09-30',
                'position' => 'Backend Programmer',
                'department_id' => 8,
                'employee_id' => 14,
                'email' => NULL,
                'username' => 'fetriant.arief',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 55,
                'name' => 'Syaifudin Latief',
                'birth_place' => '',
                'birth_date' => '1990-05-20',
                'join_date' => '2015-11-02',
                'position' => 'Back-end Programmer',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'syaifudin.latief',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 56,
                'name' => 'Alfian Asdianto',
                'birth_place' => 'Jakarta',
                'birth_date' => '1992-11-04',
                'join_date' => '2015-09-21',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 6,
                'email' => NULL,
                'username' => 'alfian.asdianto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 60,
                'name' => 'Muhammad Hawarie Kautsar',
                'birth_place' => 'jakarta',
                'birth_date' => '1998-06-05',
                'join_date' => '2015-11-23',
                'position' => 'Graphic Designer',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => NULL,
                'username' => 'muhammad.hawarie',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 61,
                'name' => 'Astiti Ratya Prawitasari I.',
                'birth_place' => 'Depok',
                'birth_date' => '1993-01-12',
                'join_date' => '2015-10-20',
                'position' => 'Graphic Designer',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => NULL,
                'username' => 'astiti.ratya',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 62,
                'name' => 'Muhamad Fahrij',
                'birth_place' => 'Bojong Gede',
                'birth_date' => '1998-01-02',
                'join_date' => '2015-12-01',
                'position' => 'Graphic Designer',
                'department_id' => 10,
                'employee_id' => 11,
                'email' => NULL,
                'username' => 'muhamad.fahrij',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 64,
                'name' => 'Fajrulaz',
                'birth_place' => '',
                'birth_date' => '2015-12-01',
                'join_date' => '2015-12-01',
                'position' => 'Programmer',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'fajrulaz',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 65,
                'name' => 'Danu sancoLgates',
                'birth_place' => '',
                'birth_date' => '2015-12-01',
                'join_date' => '2015-12-01',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 14,
                'email' => NULL,
                'username' => 'danu.sancolgates',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 66,
                'name' => 'Cinthya Edwina',
                'birth_place' => 'Jakarta',
                'birth_date' => '1990-02-18',
                'join_date' => '2015-08-24',
                'position' => 'Technical Writer',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'cinthya.edwina',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 100,
                'name' => 'Indra Jiwana Thira',
                'birth_place' => 'Ciamis ',
                'birth_date' => '1985-10-02',
                'join_date' => '2016-03-14',
                'position' => 'Project Manager',
                'department_id' => 8,
                'employee_id' => 6,
                'email' => NULL,
                'username' => 'indra.jiwana',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 68,
                'name' => 'Erlina Nur',
                'birth_place' => '',
                'birth_date' => '2016-01-01',
                'join_date' => '2016-01-01',
                'position' => 'PM Assistant',
                'department_id' => 8,
                'employee_id' => 100,
                'email' => NULL,
                'username' => 'erlina.nur',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 67,
                'name' => 'Endah Tri Wulandari Kusindaryanti',
                'birth_place' => 'Bogor',
                'birth_date' => '1993-07-01',
                'join_date' => '2015-11-09',
                'position' => 'QA',
                'department_id' => 8,
                'employee_id' => 68,
                'email' => NULL,
                'username' => 'endah.tri',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 69,
                'name' => 'Novita Dwi Handayani',
                'birth_place' => 'Tangerang',
                'birth_date' => '1994-11-08',
                'join_date' => '2014-08-04',
                'position' => 'PM Assistant',
                'department_id' => 8,
                'employee_id' => 100,
                'email' => NULL,
                'username' => 'novita.dwi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 72,
                'name' => 'Aprianti Suryani',
                'birth_place' => 'Tangerang',
                'birth_date' => '1992-04-13',
                'join_date' => '2013-10-02',
                'position' => 'Assistant PM',
                'department_id' => 8,
                'employee_id' => 100,
                'email' => NULL,
                'username' => 'aprianti.suryani',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 70,
                'name' => 'Nurafifah Winadyanata',
                'birth_place' => 'Jakarta',
                'birth_date' => '1993-12-08',
                'join_date' => '2015-12-03',
                'position' => 'QA',
                'department_id' => 8,
                'employee_id' => 72,
                'email' => NULL,
                'username' => 'nurafifah.winadyanata',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 71,
                'name' => 'Yosua Susanto',
                'birth_place' => 'Solo',
                'birth_date' => '1991-01-01',
                'join_date' => '2015-12-14',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 23,
                'email' => NULL,
                'username' => 'yosua.susanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 75,
                'name' => 'Heni Afriani',
                'birth_place' => '',
                'birth_date' => '1991-06-14',
                'join_date' => '2014-07-16',
                'position' => 'QA',
                'department_id' => 8,
                'employee_id' => 72,
                'email' => NULL,
                'username' => 'heni.afriani',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 76,
                'name' => 'Merlin Palentine Sidabutar',
                'birth_place' => 'Ambarita',
                'birth_date' => '1994-06-27',
                'join_date' => '2015-11-09',
                'position' => 'QA',
                'department_id' => 8,
                'employee_id' => 72,
                'email' => NULL,
                'username' => 'merlin.palentine',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 77,
                'name' => 'Agung Dwi Prasetyo',
                'birth_place' => 'jakarta',
                'birth_date' => '1996-04-01',
                'join_date' => '2014-09-10',
                'position' => 'office boy',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'agung.dwi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 79,
                'name' => 'Nur Alfian ',
                'birth_place' => 'Ciamis',
                'birth_date' => '1996-05-01',
                'join_date' => '2015-04-27',
                'position' => 'office boy',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'nur.alfian',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 81,
                'name' => 'Ahmad Fikri Masyhur',
                'birth_place' => 'Beijing',
                'birth_date' => '1996-02-01',
                'join_date' => '2016-02-01',
                'position' => 'Programmer',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'ahmad.fikri',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 82,
                'name' => 'Rizka Dwanira Pratiwi',
                'birth_place' => 'Namlea',
                'birth_date' => '1993-12-20',
                'join_date' => '2016-02-01',
                'position' => 'QA',
                'department_id' => 8,
                'employee_id' => 68,
                'email' => NULL,
                'username' => 'rizka.dwanira',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 83,
                'name' => 'Imam Efriadi Rizki',
                'birth_place' => 'Jakarta',
                'birth_date' => '1974-04-16',
                'join_date' => '2016-02-09',
                'position' => 'Project Manager',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'imam.efriadi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 84,
                'name' => 'Wim Sonevel',
                'birth_place' => 'Pekanbaru',
                'birth_date' => '1993-08-01',
                'join_date' => '2016-02-01',
                'position' => 'Mobile Programmer',
                'department_id' => 8,
                'employee_id' => 26,
                'email' => NULL,
                'username' => 'wim.sonevel',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 85,
                'name' => 'Didi Roesmana',
                'birth_place' => '--',
                'birth_date' => '2016-02-25',
                'join_date' => '2016-02-25',
                'position' => 'programmer',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'didi.roesmana',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 86,
                'name' => 'Mega Sylvia Wijaya',
                'birth_place' => 'Depok',
                'birth_date' => '1993-06-13',
                'join_date' => '2016-03-01',
                'position' => 'Staff GA',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'mega.sylvia',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 89,
                'name' => 'Wahyudi',
                'birth_place' => 'Bogor',
                'birth_date' => '1987-07-18',
                'join_date' => '2016-03-21',
                'position' => 'office boy',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'wahyudi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 91,
                'name' => 'Subano',
                'birth_place' => 'Lampung',
                'birth_date' => '1987-06-10',
                'join_date' => '2016-03-01',
                'position' => 'office boy',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'subano',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 92,
                'name' => 'Fitri Widi Astuti',
                'birth_place' => 'Banjar',
                'birth_date' => '1997-03-25',
                'join_date' => '2016-03-04',
                'position' => 'Staff GA',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'fitri.widi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 93,
                'name' => 'Dyah Citra Wardhani',
                'birth_place' => 'Jakarta',
                'birth_date' => '1992-03-08',
                'join_date' => '2016-02-22',
                'position' => 'Quality Assurance',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'dyah.citra',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 94,
                'name' => 'Toba Pramudia',
                'birth_place' => 'Tangerang',
                'birth_date' => '1990-04-05',
                'join_date' => '2016-02-15',
                'position' => 'System Administrator',
                'department_id' => 13,
                'employee_id' => 52,
                'email' => NULL,
                'username' => 'toba.pramudia',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 97,
                'name' => 'Rio Alamanda',
                'birth_place' => 'Pontianak',
                'birth_date' => '2016-04-01',
                'join_date' => '2016-04-04',
                'position' => 'Programmer',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'rio.alamanda',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 98,
                'name' => 'Riza Perdamaian',
                'birth_place' => 'Sungai Padi',
                'birth_date' => '1992-11-19',
                'join_date' => '2016-03-07',
                'position' => 'Quality Assurance',
                'department_id' => 8,
                'employee_id' => 69,
                'email' => NULL,
                'username' => 'riza.perdamaian',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 101,
                'name' => 'Mada Susilowati',
                'birth_place' => 'Sultra',
                'birth_date' => '1992-01-01',
                'join_date' => '2015-12-15',
                'position' => 'Admin Umum',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'mada.susilowati',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 102,
                'name' => 'Richard Oktovianus',
                'birth_place' => 'Jakarta',
                'birth_date' => '1994-10-07',
                'join_date' => '2016-04-04',
                'position' => 'Programmer',
                'department_id' => 8,
                'employee_id' => 13,
                'email' => NULL,
                'username' => 'richard.oktovianus',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 104,
                'name' => 'Aryo Eko Prasetyo',
                'birth_place' => 'Boyolali',
                'birth_date' => '1992-07-20',
                'join_date' => '2016-03-01',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 13,
                'email' => NULL,
                'username' => 'aryo.eko',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 105,
                'name' => 'Riki Novianto',
                'birth_place' => 'Boyolali',
                'birth_date' => '1995-12-30',
                'join_date' => '2016-03-01',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 9,
                'email' => NULL,
                'username' => 'riki.novianto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 106,
                'name' => 'Hendi Ardiansyah',
                'birth_place' => 'Garut',
                'birth_date' => '1989-08-20',
                'join_date' => '2016-03-01',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 23,
                'email' => NULL,
                'username' => 'hendi.ardiansyah',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 107,
                'name' => 'Demmy Dwi Rhamadan',
                'birth_place' => 'Tanggerang',
                'birth_date' => '1993-04-08',
                'join_date' => '2016-04-18',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 26,
                'email' => NULL,
                'username' => 'demmy.dwi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 108,
                'name' => 'Arvid Theodorus',
                'birth_place' => 'Nanga Pinoh',
                'birth_date' => '1990-09-01',
                'join_date' => '2016-04-18',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 23,
                'email' => NULL,
                'username' => 'arvid.theodorus',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 109,
                'name' => 'Aditya Warman',
                'birth_place' => 'Jakarta',
                'birth_date' => '1997-07-28',
                'join_date' => '2016-03-21',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 14,
                'email' => NULL,
                'username' => 'aditya.warman',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 110,
                'name' => 'Agung Suryatno',
                'birth_place' => 'Pahayu',
                'birth_date' => '1989-09-06',
                'join_date' => '2011-11-07',
                'position' => 'office boy',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'agung.suryanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 111,
                'name' => 'Sugeng Apriyono',
                'birth_place' => 'Banyumas',
                'birth_date' => '1994-05-19',
                'join_date' => '2016-04-04',
                'position' => 'office boy',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'sugeng.apriyono',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 112,
                'name' => 'Andika Fatono',
                'birth_place' => 'Malang',
                'birth_date' => '1999-06-06',
                'join_date' => '2016-05-23',
                'position' => 'programmer',
                'department_id' => 8,
                'employee_id' => 14,
                'email' => NULL,
                'username' => 'andika.fatono',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 113,
                'name' => 'Triana Nopitasari',
                'birth_place' => 'Jakarta',
                'birth_date' => '1994-11-12',
                'join_date' => '2016-04-26',
                'position' => 'Quality Assurance',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'triana.nopitasari',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 114,
                'name' => 'Raden Dwiyanto',
                'birth_place' => 'Jakarta',
                'birth_date' => '1989-02-11',
                'join_date' => '2016-06-01',
                'position' => 'IT Support',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'raden.dwiyanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 115,
                'name' => 'Zolandia Ramanda',
                'birth_place' => 'Jakarta',
                'birth_date' => '1994-03-04',
                'join_date' => '2016-06-06',
                'position' => 'Quality Assurance',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'zolandia.ramanda',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 116,
                'name' => 'Insya Ayu Treesna',
                'birth_place' => 'Kuningan',
                'birth_date' => '1993-04-03',
                'join_date' => '2016-07-11',
                'position' => 'Personal Assistant',
                'department_id' => 13,
                'employee_id' => 52,
                'email' => NULL,
                'username' => 'insya.ayu',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 117,
                'name' => 'Bambang Hermanto',
                'birth_place' => 'Jakarta',
                'birth_date' => '1980-06-06',
                'join_date' => '2015-07-24',
                'position' => 'Operasional',
                'department_id' => 14,
                'employee_id' => 36,
                'email' => NULL,
                'username' => 'bambang.hermanto',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 118,
                'name' => 'Fery Budi Jatmiko',
                'birth_place' => 'Surakarta',
                'birth_date' => '0000-00-00',
                'join_date' => '2016-06-01',
                'position' => 'Project Manager',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'fery.budi',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 119,
                'name' => 'Eggi Safarini',
                'birth_place' => 'Bandung',
                'birth_date' => '2016-06-20',
                'join_date' => '2016-06-01',
                'position' => 'Programmer (front end)',
                'department_id' => 17,
                'employee_id' => 16,
                'email' => NULL,
                'username' => 'eggi.safarini',
                'password' => Hash::make('rahasia')
            ] ,
            [
                'id' => 123,
                'name' => 'Budi Supratman',
                'birth_place' => 'Padang',
                'birth_date' => '1971-02-27',
                'join_date' => '2016-08-01',
                'position' => 'Pre- Sales',
                'department_id' => 3,
                'employee_id' => 34,
                'email' => NULL,
                'username' => 'budi.supratman',
                'password' => Hash::make('rahasia')
            ] ,
        );
        
        foreach ($employees as $employee)
        {
            Employee::create($employee);
        }
        
        $companies = array(
            [
                'id' => 1,
                'prefix' => 'DT',
                'company_name' => 'PT Docotel Teknologi'
            ] ,
            [
                'id' => 2,
                'prefix' => 'PS',
                'company_name' => 'PT Pabrik Solusi'
            ] ,
            [
                'id' => 3,
                'prefix' => 'CPU',
                'company_name' => 'PT Cakra Paramita Utama'
            ] ,
            [
                'id' => 4,
                'prefix' => 'IDC',
                'company_name' => 'PT Interdata Conection'
            ] ,
            [
                'id' => 5,
                'prefix' => 'WI',
                'company_name' => 'PT Webgis Indonesia'
            ] ,
            [
                'id' => 6,
                'prefix' => 'AP',
                'company_name' => 'CV Anastasia Putri'
            ] ,
            [
                'id' => 7,
                'prefix' => 'IKI',
                'company_name' => 'PT Inti Konsultan Indonesia'
            ] ,
            [
                'id' => 8,
                'prefix' => 'DCA',
                'company_name' => 'PT Delima Cahaya Abadi'
            ] ,
            [
                'id' => 9,
                'prefix' => 'GTP',
                'company_name' => 'CV Grace Tree Putri'
            ] ,
            [
                'id' => 10,
                'prefix' => 'GHJ',
                'company_name' => 'CV Grafika Harapan Jaya'
            ] ,
            [
                'id' => 11,
                'prefix' => 'KCT',
                'company_name' => 'PT Kreasi Cakrawala Teknologi Indonesia'
            ] ,
            [
                'id' => 12,
                'prefix' => 'RS',
                'company_name' => 'CV Regaria Sonata'
            ] ,
            [
                'id' => 13,
                'prefix' => 'RDJ',
                'company_name' => 'PT Rimpa Domafit Jaya'
            ] ,
            [
                'id' => 14,
                'prefix' => 'RJ',
                'company_name' => 'CV Riastama Jaya'
            ] ,
            [
                'id' => 15,
                'prefix' => 'CSI',
                'company_name' => 'PT Citra Sarana Infotama'
            ] ,
            [
                'id' => 16,
                'prefix' => 'TIM',
                'company_name' => 'CV Telaga Indah Mandiri'
            ] ,
            [
                'id' => 17,
                'prefix' => 'TKG',
                'company_name' => 'PT Tota Karya Givanni'
            ] ,
            [
                'id' => 18,
                'prefix' => 'LCI',
                'company_name' => 'PT Linkincube Indonesia'
            ] ,
            [
                'id' => 19,
                'prefix' => 'CDC',
                'company_name' => 'PT Citra Dinar Cahaya'
            ] ,
            [
                'id' => 20,
                'prefix' => 'CWK',
                'company_name' => 'PT Citra Wahana Konsultan'
            ] ,
            [
                'id' => 21,
                'prefix' => 'HP',
                'company_name' => 'CV Handayani Prima'
            ] ,
            [
                'id' => 22,
                'prefix' => 'DLT',
                'company_name' => 'PT Delima Laksana Tata'
            ] ,
            [
                'id' => 23,
                'prefix' => 'BU',
                'company_name' => 'CV Billy Utama'
            ] ,
            [
                'id' => 24,
                'prefix' => 'BM',
                'company_name' => 'PT Bumi Madani'
            ] ,
            [
                'id' => 25,
                'prefix' => 'GT',
                'company_name' => 'PT Gartom'
            ] ,
            [
                'id' => 26,
                'prefix' => 'SIL',
                'company_name' => 'PT Sari Intan Lestari'
            ] ,
            [
                'id' => 27,
                'prefix' => 'AAS',
                'company_name' => 'PT Aya Agape Setiawan'
            ] ,
            [
                'id' => 28,
                'prefix' => 'SCA',
                'company_name' => 'PT Santi Cahaya Abadi'
            ] ,
            [
                'id' => 29,
                'prefix' => 'APP',
                'company_name' => 'CV Azra Putri Prathama'
            ] ,
            [
                'id' => 30,
                'prefix' => 'BMU',
                'company_name' => 'PT Bonion Mitra Utama'
            ] ,
            [
                'id' => 31,
                'prefix' => 'ILJ',
                'company_name' => 'CV Inti Lestari Jaya'
            ] ,
            [
                'id' => 32,
                'prefix' => 'APR',
                'company_name' => 'CV Abdi Pratama'
            ] ,
            [
                'id' => 33,
                'prefix' => 'PVN',
                'company_name' => 'PT. PRATAMA VISI NUSA'
            ] ,
            [
                'id' => 34,
                'prefix' => 'ID',
                'company_name' => 'PT Indonusa Dwitama'
            ] ,
            [
                'id' => 35,
                'prefix' => 'KSG',
                'company_name' => 'PT. Kelola Santa Graha'
            ] ,
            [
                'id' => 36,
                'prefix' => 'MIK',
                'company_name' => 'PT. Megah Indoluksuria Komunikasi'
            ] ,
            [
                'id' => 37,
                'prefix' => 'PT OAK',
                'company_name' => 'PT. OAK Atria (pak robet)'
            ] ,
            [
                'id' => 38,
                'prefix' => 'FPS',
                'company_name' => 'PT FAJAR PRIMA SUKSES'
            ] ,
        );
        
        foreach ($companies as $company)
        {
            Company::create($company);
        }
        
        $clients = array(
            [
                'id' => 1,
                'prefix' => 'AHU',
                'client_name' => 'Direktorat Jenderal Administrasi Hukum Umum'
            ] ,
            [
                'id' => 2,
                'prefix' => 'BBPPT',
                'client_name' => 'Balai Besar Pengujian Perangkat Telekomunikasi'
            ] ,
            [
                'id' => 3,
                'prefix' => 'DJKN',
                'client_name' => 'Direktorat Jenderal Kekayaan Negara'
            ] ,
            [
                'id' => 4,
                'prefix' => 'BLBKP',
                'client_name' => 'Sekretariat Badan Penelitian & Pengembangan Kelautan & Perikanan'
            ] ,
            [
                'id' => 5,
                'prefix' => 'DJP',
                'client_name' => 'Direktorat Jenderal Pajak'
            ] ,
            [
                'id' => 6,
                'prefix' => 'IMIGRASI',
                'client_name' => 'Kantor Imigrasi Kelas I Tangerang'
            ] ,
            [
                'id' => 7,
                'prefix' => 'KEMENTAN',
                'client_name' => 'Kementerian Pertanian'
            ] ,
            [
                'id' => 8,
                'prefix' => 'AIPJ',
                'client_name' => 'Australia Indonesia Partnership for Justice'
            ] ,
            [
                'id' => 9,
                'prefix' => 'DJPPR',
                'client_name' => 'Direktorat Jenderal Pengelolaan Pembiayaan dan Risiko'
            ] ,
            [
                'id' => 10,
                'prefix' => 'SHR',
                'client_name' => 'PT Kelola Santana Graha QQ Singgasana Hotels and Resorts'
            ] ,
            [
                'id' => 11,
                'prefix' => 'BNI',
                'client_name' => 'Bank Negara Indonesia'
            ] ,
            [
                'id' => 12,
                'prefix' => 'DT',
                'client_name' => 'Docotel Teknologi'
            ] ,
            [
                'id' => 13,
                'prefix' => 'BPHN',
                'client_name' => 'BADAN PEMBINAAN HUKUM NASIONAL'
            ] ,
            [
                'id' => 14,
                'prefix' => 'PVN',
                'client_name' => 'PT. PRATAMA VISI NUSA'
            ] ,
            [
                'id' => 15,
                'prefix' => 'DJPB',
                'client_name' => 'Direktorat Jenderal Perbendaharaan'
            ] ,
            [
                'id' => 16,
                'prefix' => 'TELKOM',
                'client_name' => 'TELKOM'
            ] ,
            [
                'id' => 17,
                'prefix' => 'PT. MDM',
                'client_name' => 'PT. METRO DIGITAL MEDIA'
            ] ,
            [
                'id' => 18,
                'prefix' => 'DEPTAN',
                'client_name' => 'Departmen Pertanian'
            ] ,
            [
                'id' => 19,
                'prefix' => 'MAKARIM',
                'client_name' => 'Makarim & Taira S'
            ] ,
            [
                'id' => 20,
                'prefix' => 'ITJEN ',
                'client_name' => 'Inspektorat Jenderal Kementerian Hukum dan HAM'
            ] ,
            [
                'id' => 21,
                'prefix' => 'SGSN',
                'client_name' => 'Singgasana'
            ] ,
            [
                'id' => 22,
                'prefix' => 'IB',
                'client_name' => 'Indobast'
            ] ,
            [
                'id' => 23,
                'prefix' => 'ESDM',
                'client_name' => 'Kementrian Energi dan Sumber Daya Manusia'
            ] ,
            [
                'id' => 24,
                'prefix' => 'MIK',
                'client_name' => 'PT MEGAH INDOLUKSURIA KOMUNIKASI'
            ] ,
            [
                'id' => 25,
                'prefix' => 'Panasonic',
                'client_name' => 'PT Panasonic Healthcare Indonesia'
            ] ,
            [
                'id' => 26,
                'prefix' => 'DKH',
                'client_name' => 'Direktorat Kesehatan Hewan'
            ] ,
            [
                'id' => 27,
                'prefix' => 'KEMENKEU',
                'client_name' => 'Kementrian Keuangan'
            ] ,
            [
                'id' => 28,
                'prefix' => 'PT KAI',
                'client_name' => 'PT.KAI/KCJ'
            ] ,
            [
                'id' => 29,
                'prefix' => 'SEKJEN',
                'client_name' => 'Sekretaris Jenderal Kementerian Hukum dan HAM RI'
            ] ,
            [
                'id' => 30,
                'prefix' => 'POLRI',
                'client_name' => 'Kepolisian Republik Indonesia'
            ] ,
            [
                'id' => 31,
                'prefix' => 'IMIGRASI',
                'client_name' => 'Kantor Imigrasi Kota Depok'
            ] ,
            [
                'id' => 32,
                'prefix' => 'HKI',
                'client_name' => 'Direktorat Jenderal Hak Kekayaan Intelektual - Kemenkumham'
            ] ,
            [
                'id' => 33,
                'prefix' => 'BPN',
                'client_name' => 'Kementerian Agraria dan Tata Ruang/BPN'
            ] ,
            [
                'id' => 34,
                'prefix' => 'NUS',
                'client_name' => 'Indonusa Dwitama'
            ] ,
            [
                'id' => 35,
                'prefix' => 'IMIGRASI',
                'client_name' => 'Kantor imigrasi Jakarta Pusat'
            ] ,
            [
                'id' => 37,
                'prefix' => 'Kab.Sopeng',
                'client_name' => 'Kabupaten Sopeng'
            ] ,
            [
                'id' => 38,
                'prefix' => 'SPX',
                'client_name' => 'PT Syspex Kemasindo'
            ] ,
            [
                'id' => 39,
                'prefix' => 'VJM',
                'client_name' => 'PT. Visi Jaya Mandiri'
            ] ,
            [
                'id' => 40,
                'prefix' => 'KP',
                'client_name' => 'PT Karakatau Posco'
            ] ,
            [
                'id' => 41,
                'prefix' => 'Pemprov DK',
                'client_name' => 'Pemprov DKI'
            ] ,
            [
                'id' => 42,
                'prefix' => 'Dishub',
                'client_name' => 'Dinas Perhubungan'
            ] ,
            [
                'id' => 43,
                'prefix' => 'Kemendagri',
                'client_name' => 'Kementerian Dalam Negeri'
            ] ,
            [
                'id' => 44,
                'prefix' => 'BKK',
                'client_name' => 'Ditjen Bimas Katolik Kemenag RI'
            ] ,
            [
                'id' => 45,
                'prefix' => 'Kota ADM',
                'client_name' => 'Suku Dinas Petamanan Kota ADM Jakarta'
            ] ,
            [
                'id' => 46,
                'prefix' => 'Kemen KP',
                'client_name' => 'Kementerian Kelautan dan Perikanan'
            ] ,
            [
                'id' => 47,
                'prefix' => 'PCI',
                'client_name' => 'PT Praweda Ciptakarsa Informatika'
            ] ,
            [
                'id' => 48,
                'prefix' => 'DT',
                'client_name' => 'Dinas Tataruang'
            ] ,
            [
                'id' => 49,
                'prefix' => 'Kab. BT',
                'client_name' => 'Dinas Perhubungan kabupaten Belitung Timur'
            ] ,
            [
                'id' => 50,
                'prefix' => 'ESDM',
                'client_name' => 'Kementerian Energi dan Sumber Daya Mineral'
            ] ,
        );
        
        foreach ($clients as $client)
        {
            Client::create($client);
        }
        
        $projects = array(
            [
                'id' => 3,
                'project_name' => 'Operasional',
                'client_id' => 12,
                'company_id' => 1,
                'department_id' => 14,
                'limit' => 10000000.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 800,
                'project_name' => 'DJPPR',
                'client_id' => 9,
                'company_id' => 2,
                'department_id' => 10,
                'limit' => 54399400.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 49,
                'project_name' => 'Pemeliharaan ePasti',
                'client_id' => 1,
                'company_id' => 1,
                'department_id' => 8,
                'limit' => 200000000.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 1,
                'project_name' => 'In2luxury',
                'client_id' => 12,
                'company_id' => 36,
                'department_id' => 8,
                'limit' => 100000000.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 2,
                'project_name' => 'Pemeliharaan Website Maintenance Imigrasi Tangerang',
                'client_id' => 6,
                'company_id' => 1,
                'department_id' => 3,
                'limit' => 29040000.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 48,
                'project_name' => 'Pengadaan Modul Monitoring dan Evaluas Aplikasi Kesekretariatan',
                'client_id' => 1,
                'company_id' => 21,
                'department_id' => 8,
                'limit' => 193347000.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 47,
                'project_name' => 'Pengadaan Aplikasi Pelaporan Online Untuk Pusat, Kanwil dan BHP',
                'client_id' => 1,
                'company_id' => 30,
                'department_id' => 8,
                'limit' => 175461000.00,
                'status' => false,
                'year' => 2016,
            ] ,
            [
                'id' => 46,
                'project_name' => 'Pengadaan Aplikasi Pelayanan Kurator',
                'client_id' => 1,
                'company_id' => 1,
                'department_id' => 8,
                'limit' => 176671000.00,
                'status' => false,
                'year' => 2016,
            ] ,
        );
        
        foreach ($projects as $project)
        {
            Project::create($project);
        }
        
        $overtime_types = array(
            [
                'id' => 1,
                'overtime_type' => 'Weekend',
            ] ,
            [
                'id' => 2,
                'overtime_type' => 'Weekday',
            ] ,
            [
                'id' => 3,
                'overtime_type' => 'Holiday',
            ] ,
        );
        
        foreach ($overtime_types as $overtime_type)
        {
            overtime_type::create($overtime_type);
        }
        
        $overtimes = array(
             [
                 'id' => 1,
                 'overtime_type_id' => 1,
                 'employee_id' => 8,
                 'project_id' => 46,
                 'date' => '2016-08-09',
                 'clock_in' => '09:00:00',
                 'clock_out' => '18:00:00',
                 'created_by'=> 6,
                 'approved' => null,
             ] ,
             [
                 'id' => 2,
                 'overtime_type_id' => 2,
                 'employee_id' => 34,
                 'project_id' => 47,
                 'date' => '2016-07-16',
                 'clock_in' => '19:00:00',
                 'clock_out' => '23:00:00',
                 'created_by' => 6,
                 'approved' => null,
             ] ,
             [
                 'id' => 3,
                 'overtime_type_id' => 3,
                 'employee_id' => 101,
                 'project_id' => 48,
                 'date' => '2016-08-17',
                 'clock_in' => '07:00:00',
                 'clock_out' => '15:00:00',
                 'created_by' => 6,
                 'approved' => null,
             ] ,
         );
         
         foreach ($overtimes as $overtime)
         {
            Overtime::create($overtime);
         }
    }
}
