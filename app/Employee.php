<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'join_date', 'position', 'birth_place', 'birth_date', 'employee_id', 'department_id', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'username', 'created_at', 'updated_at', 'department_id', 'employee_id',
    ];
    
    public function Department()
    {
        return $this->belongsTo('App\Department');
    }
    
    public function Downline()
    {
        return $this->hasMany('App\Employee');
    }
    
    public function Toplevel()
    {
        return $this->belongsTo('App\Employee', 'employee_id');
    }
}
