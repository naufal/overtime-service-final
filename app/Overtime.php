<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overtime extends Model
{    
    protected $fillable = [ 'overtime_type_id', 'employee_id', 'project_id', 'date', 'clock_in', 'clock_out', 'created_by', ];
    
    public function OvertimeType()
    {
        return $this->belongsTo('App\overtime_type');
    }
    
    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }
    
    public function Project()
    {
        return $this->belongsTo('App\Project');
    }
    
    public function CreatedBy()
    {
        return $this->belongsTo('App\Employee', 'created_by', 'id');
    }
}
