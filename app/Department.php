<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'id', 'prefix', 'department_name',
    ];
    public $timestamps = false;
    
    public function Employees()
    {
        return $this->hasMany('App\Employee');
    }
}
