<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Overtime;

use Auth;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class OvertimeController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate']]);
    }
    
    public function fetch(Request $request)
    {
        return Overtime::paginate(15);
    }
    
    public function get(Request $request, $id)
    {
        return Overtime::with('OvertimeType')->with('Employee')->with('Project')->with('CreatedBy')->findOrFail($id);
    }
    
    public function post(Request $request)
    {
        $overtime = [            
            'overtime_type_id' => (int) $request->input('overtime_type_id'),
            'employee_id' => (int) $request->input('employee_id'),
            'project_id' => (int) $request->input('project_id'),
            'date' => $request->input('date'),
            'clock_in' => $request->input('clock_in'),
            'clock_out' => $request->input('clock_out'),
            'approved' => null,
            'created_by' => Auth::user()->id,
        ];
        return Overtime::create($overtime);
    }
    
    public function put(Request $request, $id)
    {
        $overtime = Overtime::findOrFail($id);
        if ($request->has('overtime_type_id'))
        {
            $overtime->overtime_type_id = $request->input('overtime_type_id');
        }        
        if ($request->has('employee_id'))
        {
            $overtime->employee_id = $request->input('employee_id');
        }        
        if ($request->has('project_id'))
        {
            $overtime->project_id = $request->input('project_id');
        }
        if ($request->has('date'))
        {
            $overtime->date = $request->input('date');
        }
        if ($request->has('clock_in'))
        {
            $overtime->clock_in = $request->input('clock_in');
        }
        if ($request->has('clock_out'))
        {
            $overtime->clock_out = $request->input('clock_out');
        }
        
        if ($overtime->save())
        {
            return array('success' => true);
        }
        else
        {
            return array('success' => false);
        }
    }
    
    public function delete(Request $request, $id)
    {
        $overtime = Overtime::findOrFail($id);
        if ($overtime->delete())
        {
            return array('success' => true);
        }
        else
        {
            return array('success' => false);
        }
    }
    
    public function approve(Request $request, $id)
    {
        if (Auth::user()->employee_id == null)
        {
            $overtime = Overtime::findOrFail($id);
            $overtime->approved = true;
            if ($overtime->save())
            {
                return array('success' => true);
            }
            else
            {
                return array('success' => false);
            }
        }
        else 
        {
            return array('success' => false, 'message' => 'You do not have permission to approve overtime.');
        }
    }
    
    public function reject(Request $request, $id)
    {
        if (Auth::user()->employee_id == null)
        {
            $overtime = Overtime::findOrFail($id);
            $overtime->approved = false;
            if ($overtime->save())
            {
                return array('success' => true);
            }
            else
            {
                return array('success' => false);
            }
        }
        else 
        {
            return array('success' => false, 'message' => 'You do not have permission to reject overtime.');
        }
    }
    
    public function weekday(Request $request)
    {
        return Overtime::where('overtime_type_id', 2)->paginate(15);
    }
    
    public function weekend(Request $request)
    {
        return Overtime::where('overtime_type_id', 1)->paginate(15);
    }
    
    public function holiday(Request $request)
    {
        return Overtime::where('overtime_type_id', 3)->paginate(15);
    }
}
