<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Company;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate']]);
    }
    
    public function fetch(Request $request)
    {
        return Company::paginate(15);
    }
    
    public function get(Request $request, $id)
    {
        return Company::findOrFail($id);
    }
}
