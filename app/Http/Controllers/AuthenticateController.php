<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate','refresh']]);
    }
    
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }
    
    public function refresh(Request $request)
    {
        $token = JWTAuth::getToken();
        
        if (!$token) {
            throw new BadRequestHtttpException('Token not provided');
        }
        
        try {
            $token = JWTAuth::refresh($token);
        }
        catch (TokenExpiredException $e) {
            throw new AccessDeniedHttpException('The token is invalid');
        }
        
        return response()->json(compact('token'));
    }
    
    public function me(Request $request)
    {
        $user = \App\User::findOrFail(Auth::user()->id);
        return response()->json($user);
    }
    
    public function toplevel(Request $request)
    {
        $user = \App\User::findOrFail(Auth::user()->id);
        $toplevel = \App\User::findOrFail($user->employee_id);
        return response()->json($toplevel);
    }
}
