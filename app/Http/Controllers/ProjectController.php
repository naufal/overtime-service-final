<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate']]);
    }
    
    public function fetch(Request $request)
    {
        return Project::paginate(15);
    }
    
    public function get(Request $request, $id)
    {
        return Project::with('Client')->with('Company')->with('Department')->findOrFail($id);
    }
}
