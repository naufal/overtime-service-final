<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employee;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate']]);
    }
    
    public function fetch(Request $request)
    {
        return Employee::paginate(15);
    }
    
    public function get(Request $request, $id)
    {
        return Employee::with('Department')->with('Downline')->with('Toplevel')->findOrFail($id);
    }
}
