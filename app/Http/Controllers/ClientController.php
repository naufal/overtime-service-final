<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Client;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate']]);
    }
    
    public function fetch(Request $request)
    {
        return Client::paginate(15);
    }
    
    public function get(Request $request, $id)
    {
        return Client::findOrFail($id);
    }
}
