<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Department;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth',['except' => ['authenticate']]);
    }
    
    public function fetch(Request $request)
    {
        return Department::all();
    }
    
    public function get(Request $request, $id)
    {
        return Department::with('Employees')->findOrFail($id);
    }
}
