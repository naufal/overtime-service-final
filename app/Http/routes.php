<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api'], function() {
	Route::group(['prefix' => 'authenticate'], function() {
        Route::post('/', 'AuthenticateController@authenticate');
        Route::get('/refresh', 'AuthenticateController@refresh');
        Route::get('/me', 'AuthenticateController@me');
        Route::get('/toplevel', 'AuthenticateController@toplevel');
	});
	Route::group(['prefix' => 'departments'], function() {
        Route::get('/', 'DepartmentController@fetch');
        Route::get('/{id}', 'DepartmentController@get');
	});
	Route::group(['prefix' => 'employees'], function() {
        Route::get('/', 'EmployeeController@fetch');
        Route::get('/{id}', 'EmployeeController@get');
	});
	Route::group(['prefix' => 'clients'], function() {
        Route::get('/', 'ClientController@fetch');
        Route::get('/{id}', 'ClientController@get');
	});
	Route::group(['prefix' => 'companies'], function() {
        Route::get('/', 'CompanyController@fetch');
        Route::get('/{id}', 'CompanyController@get');
	});
	Route::group(['prefix' => 'projects'], function() {
        Route::get('/', 'ProjectController@fetch');
        Route::get('/{id}', 'ProjectController@get');
	});
	Route::group(['prefix' => 'overtime'], function() {
        Route::get('/', 'OvertimeController@fetch');
        Route::get('/{id}', 'OvertimeController@get');
        Route::get('/weekday', 'OvertimeController@weekday');
        Route::get('/weekend', 'OvertimeController@weekend');
        Route::get('/holiday', 'OvertimeController@holiday');
        Route::post('/', 'OvertimeController@post');
        Route::put('/{id}', 'OvertimeController@put');
        Route::put('/{id}/approve', 'OvertimeController@approve');
        Route::put('/{id}/reject', 'OvertimeController@reject');
        Route::delete('/{id}', 'OvertimeController@delete');
	});
});
