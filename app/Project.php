<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function Company()
    {
        return $this->belongsTo('App\Company');
    }
    
    public function Department()
    {
        return $this->belongsTo('App\Department');
    }
    
    public function Client()
    {
        return $this->belongsTo('App\Client');
    }
    
    protected $hidden = ['company_id', 'department_id', 'client_id', 'created_at', 'updated_at', ];
}
